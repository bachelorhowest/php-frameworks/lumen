<?php

namespace App\Controller;

use App\Entity\Products;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;



class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index(): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);
        $repo = $this->getDoctrine()->getRepository(Products::class);
        $json = $serializer->serialize(array_map(function ($products) {
            return ['data' => $products]; },
            $repo->findAll()
        ),'json');
       return $this->json([
           'data' => json_decode($json)
       ]);
    }

    /**
     * @route("/products/add", name="newProduct", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function new (Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $name = $data['name'];
        $articleId = $data['articleId'] ;
        $price = $data['price'];
        $width= $data['width'];
        $height= $data['height'];
        $description = $data['description'];

        $product = new Products();
        $product->setname($name);
        $product->setprice($price);
        $product->setdescription($description);
        $product->setarticleId($articleId);
        $product->setWidth($width);
        $product->setHeight($height);

        $dt = new DateTime("NOW");
        $product->setCreatedAt($dt);
        $product->setUpdatedAt($dt);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return new JsonResponse('Product created');

    }
}
